# GWP4RSE

## Wichtige Links
* Präsentation und Fragenkatalog:  https://pad.gwdg.de/BgEScUUUTbuAAtQybpfzrw
* FAQ Sammlung aus Vorbereitung: https://pad.gwdg.de/7tuC_lYRQf2l2Stj5-TzdQ?view#FAQ-Sammlung
* MPG Richtlinien zur GWP: https://www.mpg.de/199493/regelnWissPraxis.pdf 
* DFG Richtlinien zur GWP: https://zenodo.org/record/6472827
* MPG "LeitPlancken": https://www.mpg.de/18156400/leitplancken.pdf
* Vorträge "Future Opportunities for Software in Research 2022": https://s.gwdg.de/kNxjtU (link zu PuRe)
* Open Access (Berlin Declaration): https://openaccess.mpg.de/Berlin-Declaration

## Beiträge
Einfach für eine neue FAQ ein Issue aufmachen (vorher Issues nach dieser oder ähnlicher Frage durchsuchen).
Dann die Antwort auf die Frage als Kommentar anhängen und mit Teammitgliedern diskutieren.

## Themenbereiche
### [Rechtliches](Rechtliches.md)
### [Reproduzierbarkeit](Reproduzierbarkeit.md)
### [PIDs und Zitation](PIDs.md)
### [Sicherheit und Ethik](Sicherheit-Ethik.md)
### [Repositorien](Repositorien.md)
### [Training](Training.md)
### [Dokumentation](Dokumentation.md)
### [Gelbe Seiten](GelbeSeiten.md)
